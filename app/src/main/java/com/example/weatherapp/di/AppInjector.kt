package com.example.weatherapp.di

import com.example.weatherappkunal.WeatherService
import org.koin.dsl.module
import retrofit2.Retrofit



private val retrofit: Retrofit = createNetworkClient()


private val weatherService: WeatherService = retrofit.create(WeatherService::class.java)

val networkModule = module {
    single { weatherService }
}