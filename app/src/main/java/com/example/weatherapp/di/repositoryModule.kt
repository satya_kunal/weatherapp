package com.example.weatherapp.di

import com.example.weatherapp.repository.weatherRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule=module{
    single { weatherRepository(androidContext(), weatherService = get()) }

}