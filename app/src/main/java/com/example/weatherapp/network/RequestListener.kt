package com.example.weatherapp.network

import androidx.lifecycle.LiveData
import com.example.weatherapp.weather.WeatherResponse

interface RequestListener {
    fun OnStarted()
    fun OnSuccess(weatherResponse: LiveData<WeatherResponse>)
    fun OnFailure(message:String)
}