package com.example.weatherappkunal

import com.example.weatherapp.weather.WeatherResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface WeatherService {


//    @GET("data/2.5/weather?")
//    fun getCurrentWeatherData(@Query("q") q: String, @Query("APPID") app_id: String): Call<WeatherResponse>

    @GET("data/2.5/weather?")
    fun forecast(
        @Query("q") city: String,
        @Query("appid") id: String

        ): Call<WeatherResponse>

//    companion object {
//        operator fun invoke(): WeatherService {
//            return Retrofit.Builder()
//                .baseUrl("http://api.openweathermap.org/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//                .create(WeatherService::class.java)
//        }
//    }
}

