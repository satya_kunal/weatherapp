package com.example.weatherapp.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.weatherapp.di.networkModule
import com.example.weatherapp.weather.WeatherResponse
import com.example.weatherappkunal.WeatherService
import org.koin.core.context.loadKoinModules
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class weatherRepository(private val context: Context, private val weatherService: WeatherService) {

    fun forecast(city: String,id:String): LiveData<WeatherResponse> {
        val weatherResponse = MutableLiveData<WeatherResponse>()

        weatherService.forecast(city,id)
            .enqueue(object : Callback<WeatherResponse> {
                override fun onFailure(call: Call<WeatherResponse>?, t: Throwable?) {

                    Log.d("!!! weatherRepository","response onfailure ${t!!.message} ")

                }

                override fun onResponse(
                    call: Call<WeatherResponse>?,
                    response: Response<WeatherResponse>?
                ) {
                    if (response!!.isSuccessful) {

                        Log.d("!!! weatherRepository","response success")
                        weatherResponse.value = response.body()!!
                    } else {                        Log.d("!!weatherRepository","response fail")

                        Log.d("!!! weatherRepository","response fail ${response.errorBody()?.string()}")


                    }
                }

            })
        return weatherResponse
    }
}