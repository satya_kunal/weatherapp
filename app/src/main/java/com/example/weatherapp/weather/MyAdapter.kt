package com.example.weatherappkunal

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.weather.WeatherResponse

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MyAdapter(private val list: List<RecyclerList>, private val context: Context) : RecyclerView.Adapter<MyAdapter.MyAdapterViewHolder>() {
    private val TAG = javaClass.simpleName
    private val weatherResponse: WeatherResponse? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapterViewHolder {

        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.weather, parent, false)
        return MyAdapterViewHolder(view)

    }

    override fun onBindViewHolder(holder: MyAdapterViewHolder, position: Int) {

        val recyclerList = list[position]
        Log.d(TAG, "!!! onbindview holder  city = " + recyclerList.main)
        holder.xmlcity.text = " " + recyclerList.name
        holder.xmlhumidity.text = recyclerList.humidity + "%"
        holder.weatherCondition = recyclerList.main


        if (recyclerList.description == "light rain") {
            holder.weatherImage.setImageResource(R.drawable.ligthrain)
        }
        if (recyclerList.description == "scattered clouds") {
            holder.weatherImage.setImageResource(R.drawable.scatteredcloud)
        }
        if (recyclerList.description == "sky is clear") {
            holder.weatherImage.setImageResource(R.drawable.clearsky)
        }
        if (recyclerList.description == "heavy intensity rain") {
            holder.weatherImage.setImageResource(R.drawable.heavyrain)
        }
        if (recyclerList.description == "few clouds") {
            holder.weatherImage.setImageResource(R.drawable.fewclouds)
        }
        if (recyclerList.description == "heavy intensity rain") {
            holder.weatherImage.setImageResource(R.drawable.heavyrain)
        }
        if (recyclerList.description == "moderate rain") {
            holder.weatherImage.setImageResource(R.drawable.moderaterain)
        }
        if (recyclerList.description == "broken clouds") {
            holder.weatherImage.setImageResource(R.drawable.brokenclouds)
        }
        if (recyclerList.description == "overcast clouds") {
            holder.weatherImage.setImageResource(R.drawable.overcastclouds)
        }
        if (recyclerList.description == "light shower snow") {
            holder.weatherImage.setImageResource(R.drawable.lightshowerrain)
        }
        //
        //            case "moderate rain":
        //                holder.weatherImage.setImageResource(R.drawable.moderaterain);
        //                break;
        //
        //                default:
        //                    holder.weatherImage.setImageResource(R.drawable.clearsky);
        //                    break;


        holder.xmldescription.text = recyclerList.description
        holder.xmlcountry.text = "," + recyclerList.country
        holder.xmltemp.text = "  " + recyclerList.temp + "C"
        holder.xmlpressure.text = "  " + recyclerList.pressure + "pa"
        holder.xmlwind.text = "  " + recyclerList.wind + "m/s"

        val currentDate = SimpleDateFormat(" EEE \n dd MMM", Locale.getDefault()).format(Date())

        holder.xmldate.text = currentDate


        Log.d(TAG, "!!! onbindview holder  Success " + recyclerList.wind)


    }


    override fun getItemCount(): Int {
        return list.size
    }


    inner class MyAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var xmlcity: TextView
        internal var xmltemp: TextView
        internal var xmldescription: TextView
        internal var xmlcountry: TextView
        internal var xmldate: TextView
        internal var xmlhumidity: TextView
        internal var xmlwind: TextView
        internal var xmlpressure: TextView
        internal var weatherImage: ImageView
        internal var weatherCondition: String? = null

        init {

            xmlcity = itemView.findViewById(R.id.xml_city)
            xmltemp = itemView.findViewById(R.id.xml_temp)
            weatherImage = itemView.findViewById(R.id.xml_image)
            xmlcountry = itemView.findViewById(R.id.xml_country)
            xmldescription = itemView.findViewById(R.id.xml_description)
            xmldate = itemView.findViewById(R.id.xml_date)
            xmlhumidity = itemView.findViewById(R.id.xml_humidity)
            xmlwind = itemView.findViewById(R.id.xml_wind)
            xmlpressure = itemView.findViewById(R.id.xml_pressure)

        }
    }

}
