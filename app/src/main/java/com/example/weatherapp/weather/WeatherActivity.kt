package com.example.weatherapp.weather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.Utils.hide
import com.example.weatherapp.Utils.show
import com.example.weatherapp.Utils.toast
import com.example.weatherapp.network.RequestListener
import com.example.weatherappkunal.MyAdapter
import com.example.weatherappkunal.RecyclerList
import kotlinx.android.synthetic.main.weather_activity.*
import java.util.ArrayList
import org.koin.android.viewmodel.ext.android.viewModel


class WeatherActivity : AppCompatActivity(), RequestListener {
    private var recyclerView: RecyclerView? = null
    private val items = ArrayList<RecyclerList>()
    private val tag="WeatherActivity"
    private val mainViewModel: WeatherModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.weather_activity)



        mainViewModel.requestListener = this
        recyclerView = findViewById(R.id.rv)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = MyAdapter(items, this)



        button_search.setOnClickListener{
            mainViewModel.cityName=tv_city_name.text.toString()
            Log.d(tag,"!!! cityname ${mainViewModel.cityName}")
            Log.d(tag,"!!! cityname ${tv_city_name.text.toString()}")
            Log.d(tag,"!!! clicked")
            mainViewModel.provideForecast()
        }
    }

    override fun OnStarted() {
        toast("Started")
        progress_bar.show()
    }

    override fun OnSuccess(response: LiveData<WeatherResponse>) {
        toast("Success")
        Handler().postDelayed(
            {
                progress_bar.hide()
                response.observe(this, Observer {
                    val recyclerList = RecyclerList(
                        it.sys.country,
                        Math.round(it.main.temp / 10).toString(),
                        it.name,
                        it.main.humidity.toString(),
                        it.weather[0].description,
                        it.weather[0].main,
                        it.wind.speed.toString(),
                        it.main.pressure.toString()
                    )

                    items.add(recyclerList)
                    recyclerView!!.adapter!!.notifyItemInserted(items.size - 1)
                    recyclerView!!.smoothScrollToPosition(items.size - 1)
                    toast(it.name)
                })
            },
            1000
        )


    }

    override fun OnFailure(message: String) {
        toast(message)
        progress_bar.hide()
    }



}
