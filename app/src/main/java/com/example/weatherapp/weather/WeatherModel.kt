package com.example.weatherapp.weather

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.example.weatherapp.network.RequestListener
import com.example.weatherapp.repository.weatherRepository
import com.example.weatherappkunal.WeatherService

class WeatherModel(private val weatherRepo: weatherRepository): ViewModel(){

    var cityName:String?=null

    var requestListener: RequestListener?=null

    fun provideForecast()
    {
        requestListener?.OnStarted()
        if(cityName.isNullOrEmpty())
        {
            requestListener?.OnFailure("invalid city name")
            return
        }
        val id="f7e7f4c8fd5df46353a72a3a5b67771f"
        val weatherResponse=weatherRepo.forecast(cityName!!,id)


        Log.d("!!! WeatherModel","city $cityName ")
        requestListener?.OnSuccess(weatherResponse)
    }


    fun weatherForecast(view:View){

        Log.d("model",view.toString())
        provideForecast()

    }
}