package com.example.weatherapp.weather;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherResponse {

    public WeatherResponse(String s)
    {

    }

    @SerializedName("coord")
    public Coord coord;
    @SerializedName("sys")
    public Sys sys;
    @SerializedName("weather")
    public ArrayList<Weather> weather = new ArrayList<Weather>();
    @SerializedName("main")
    public Main main;

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public float getDt() {
        return dt;
    }

    public void setDt(float dt) {
        this.dt = dt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getCod() {
        return cod;
    }

    public void setCod(float cod) {
        this.cod = cod;
    }

    @SerializedName("wind")
    public Wind wind;
    @SerializedName("rain")
    public Rain rain;
    @SerializedName("clouds")
    public Clouds clouds;
    @SerializedName("dt")
    public float dt;
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("cod")
    public float cod;
}

class Weather {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @SerializedName("id")
    public int id;
    @SerializedName("main")
    public String main;
    @SerializedName("description")
    public String description;
    @SerializedName("icon")
    public String icon;

}

class Clouds {
    @SerializedName("all")
    public float all;

    public float getAll() {
        return all;
    }

    public void setAll(float all) {
        this.all = all;
    }
}

class Rain {
    @SerializedName("3h")
    public float h3;

    public float getH3() {
        return h3;
    }

    public void setH3(float h3) {
        this.h3 = h3;
    }
}

class Wind {
    @SerializedName("speed")
    public float speed;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    @SerializedName("deg")
    public float deg;
}

class Main {
    @SerializedName("temp")
    public float temp;

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }

    @SerializedName("humidity")
    public float humidity;
    @SerializedName("pressure")
    public float pressure;
    @SerializedName("temp_min")
    public float temp_min;
    @SerializedName("temp_max")
    public float temp_max;
}

class Sys {
    @SerializedName("country")
    public String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    @SerializedName("sunrise")
    public long sunrise;
    @SerializedName("sunset")
    public long sunset;
}

class Coord {
    @SerializedName("lon")
    public float lon;

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    @SerializedName("lat")
    public float lat;
}








//    private String cod;
//    private float message;
//    City CityObject;
//    private float cnt;
//    @SerializedName("list")
//    ArrayList<ListOfData> list = new ArrayList<>();
//
//     class ListOfData{
//
//        Temp TempObject;
//        private float pressure;
//        private float humidity;
//        ArrayList < Object > weather = new ArrayList < Object > ();
//        private float speed;
//        private float deg;
//        private float clouds;
//        private float snow;
//         private float dt;
//
//         public Temp getTempObject() {
//             return TempObject;
//         }
//
//         public void setTempObject(Temp tempObject) {
//             TempObject = tempObject;
//         }
//
//         public ArrayList<Object> getWeather() {
//             return weather;
//         }
//
//         public void setWeather(ArrayList<Object> weather) {
//             this.weather = weather;
//         }
//// Getter Methods
//
//        public float getDt() {
//            return dt;
//        }
//
//        public Temp getTemp() {
//            return TempObject;
//        }
//
//        public float getPressure() {
//            return pressure;
//        }
//
//        public float getHumidity() {
//            return humidity;
//        }
//
//        public float getSpeed() {
//            return speed;
//        }
//
//        public float getDeg() {
//            return deg;
//        }
//
//        public float getClouds() {
//            return clouds;
//        }
//
//        public float getSnow() {
//            return snow;
//        }
//
//        // Setter Methods
//
//        public void setDt(float dt) {
//            this.dt = dt;
//        }
//
//        public void setTemp(Temp tempObject) {
//            this.TempObject = tempObject;
//        }
//
//        public void setPressure(float pressure) {
//            this.pressure = pressure;
//        }
//
//        public void setHumidity(float humidity) {
//            this.humidity = humidity;
//        }
//
//        public void setSpeed(float speed) {
//            this.speed = speed;
//        }
//
//        public void setDeg(float deg) {
//            this.deg = deg;
//        }
//
//        public void setClouds(float clouds) {
//            this.clouds = clouds;
//        }
//
//        public void setSnow(float snow) {
//            this.snow = snow;
//        }
//    }
//    public class Temp {
//        private float day;
//        private float min;
//        private float max;
//        private float night;
//        private float eve;
//        private float morn;
//
//
//        // Getter Methods
//
//        public float getDay() {
//            return day;
//        }
//
//        public float getMin() {
//            return min;
//        }
//
//        public float getMax() {
//            return max;
//        }
//
//        public float getNight() {
//            return night;
//        }
//
//        public float getEve() {
//            return eve;
//        }
//
//        public float getMorn() {
//            return morn;
//        }
//
//        // Setter Methods
//
//        public void setDay(float day) {
//            this.day = day;
//        }
//
//        public void setMin(float min) {
//            this.min = min;
//        }
//
//        public void setMax(float max) {
//            this.max = max;
//        }
//
//        public void setNight(float night) {
//            this.night = night;
//        }
//
//        public void setEve(float eve) {
//            this.eve = eve;
//        }
//
//        public void setMorn(float morn) {
//            this.morn = morn;
//        }
//    }
//
//
//    // Getter Methods
//
//    public String getCod() {
//        return cod;
//    }
//
//    public float getMessage() {
//        return message;
//    }
//
//    public City getCity() {
//        return CityObject;
//    }
//
//    public float getCnt() {
//        return cnt;
//    }
//
//    // Setter Methods
//
//    public void setCod(String cod) {
//        this.cod = cod;
//    }
//
//    public void setMessage(float message) {
//        this.message = message;
//    }
//
//    public void setCity(City cityObject) {
//        this.CityObject = cityObject;
//    }
//
//    public void setCnt(float cnt) {
//        this.cnt = cnt;
//    }
//
//    public class City {
//        private float geoname_id;
//        private String name;
//        private float lat;
//        private float lon;
//        private String country;
//        private String iso2;
//        private String type;
//        private float population;
//
//
//        // Getter Methods
//
//        public float getGeoname_id() {
//            return geoname_id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public float getLat() {
//            return lat;
//        }
//
//        public float getLon() {
//            return lon;
//        }
//
//        public String getCountry() {
//            return country;
//        }
//
//        public String getIso2() {
//            return iso2;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public float getPopulation() {
//            return population;
//        }
//
//        // Setter Methods
//
//        public void setGeoname_id(float geoname_id) {
//            this.geoname_id = geoname_id;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public void setLat(float lat) {
//            this.lat = lat;
//        }
//
//        public void setLon(float lon) {
//            this.lon = lon;
//        }
//
//        public void setCountry(String country) {
//            this.country = country;
//        }
//
//        public void setIso2(String iso2) {
//            this.iso2 = iso2;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//    }
//}








//    public WeatherResponse(String city,float temp)
//    {
//        this.main.temp=temp;
//        this.sys.country=city;
//
//    }
//    @SerializedName("coord")
//    public Coord coord;
//    @SerializedName("sys")
//    public Sys sys;
//    @SerializedName("weather")
//    public ArrayList<Weather> weather = new ArrayList<Weather>();
//    @SerializedName("main")
//    public Main main;
//    @SerializedName("list")
//    public ArrayList<ListOfData> listOfData=new ArrayList<ListOfData>();
//
//    public Coord getCoord() {
//        return coord;
//    }
//
//    public void setCoord(Coord coord) {
//        this.coord = coord;
//    }
//
//    public Sys getSys() {
//        return sys;
//    }
//
//    public void setSys(Sys sys) {
//        this.sys = sys;
//    }
//
//    public ArrayList<Weather> getWeather() {
//        return weather;
//    }
//
//    public void setWeather(ArrayList<Weather> weather) {
//        this.weather = weather;
//    }
//
//    public Main getMain() {
//        return main;
//    }
//
//    public void setMain(Main main) {
//        this.main = main;
//    }
//
//    public Wind getWind() {
//        return wind;
//    }
//
//    public void setWind(Wind wind) {
//        this.wind = wind;
//    }
//
//    public Rain getRain() {
//        return rain;
//    }
//
//    public void setRain(Rain rain) {
//        this.rain = rain;
//    }
//
//    public Clouds getClouds() {
//        return clouds;
//    }
//
//    public void setClouds(Clouds clouds) {
//        this.clouds = clouds;
//    }
//
//    public float getDt() {
//        return dt;
//    }
//
//    public void setDt(float dt) {
//        this.dt = dt;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public ArrayList<ListOfData> getListOfData() {
//        return listOfData;
//    }
//
//    public void setListOfData(ArrayList<ListOfData> listOfData) {
//        this.listOfData = listOfData;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public float getCod() {
//        return cod;
//    }
//
//    public void setCod(float cod) {
//        this.cod = cod;
//    }
//
//    @SerializedName("wind")
//    public Wind wind;
//    @SerializedName("rain")
//    public Rain rain;
//    @SerializedName("clouds")
//    public Clouds clouds;
//    @SerializedName("dt")
//    public float dt;
//    @SerializedName("id")
//    public int id;
//    @SerializedName("name")
//    public String name;
//    @SerializedName("cod")
//    public float cod;
//}
//
//class ListOfData{
//    @SerializedName("dt")
//    public double date;
//
//    public double getDate() {
//        return date;
//    }
//
//    public void setDate(double date) {
//        this.date = date;
//    }
//}
//
//class Weather {
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getMain() {
//        return main;
//    }
//
//    public void setMain(String main) {
//        this.main = main;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//
//    @SerializedName("id")
//    public int id;
//    @SerializedName("main")
//    public String main;
//    @SerializedName("description")
//    public String description;
//    @SerializedName("icon")
//    public String icon;
//}
//
//class Clouds {
//    @SerializedName("all")
//    public float all;
//
//    public float getAll() {
//        return all;
//    }
//
//    public void setAll(float all) {
//        this.all = all;
//    }
//}
//
//class Rain {
//    @SerializedName("3h")
//    public float h3;
//
//    public float getH3() {
//        return h3;
//    }
//
//    public void setH3(float h3) {
//        this.h3 = h3;
//    }
//}
//
//class Wind {
//    @SerializedName("speed")
//    public float speed;
//
//    public float getSpeed() {
//        return speed;
//    }
//
//    public void setSpeed(float speed) {
//        this.speed = speed;
//    }
//
//    public float getDeg() {
//        return deg;
//    }
//
//    public void setDeg(float deg) {
//        this.deg = deg;
//    }
//
//    @SerializedName("deg")
//    public float deg;
//}
//
//class Main {
//    @SerializedName("temp")
//    public float temp;
//
//    public float getTemp() {
//        return temp;
//    }
//
//    public void setTemp(float temp) {
//        this.temp = temp;
//    }
//
//    public float getHumidity() {
//        return humidity;
//    }
//
//    public void setHumidity(float humidity) {
//        this.humidity = humidity;
//    }
//
//    public float getPressure() {
//        return pressure;
//    }
//
//    public void setPressure(float pressure) {
//        this.pressure = pressure;
//    }
//
//    public float getTemp_min() {
//        return temp_min;
//    }
//
//    public void setTemp_min(float temp_min) {
//        this.temp_min = temp_min;
//    }
//
//    public float getTemp_max() {
//        return temp_max;
//    }
//
//    public void setTemp_max(float temp_max) {
//        this.temp_max = temp_max;
//    }
//
//    @SerializedName("humidity")
//    public float humidity;
//    @SerializedName("pressure")
//    public float pressure;
//    @SerializedName("temp_min")
//    public float temp_min;
//    @SerializedName("temp_max")
//    public float temp_max;
//}
//
//class Sys {
//    @SerializedName("country")
//    public String country;
//
//    public String getCountry() {
//        return country;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//    public long getSunrise() {
//        return sunrise;
//    }
//
//    public void setSunrise(long sunrise) {
//        this.sunrise = sunrise;
//    }
//
//    public long getSunset() {
//        return sunset;
//    }
//
//    public void setSunset(long sunset) {
//        this.sunset = sunset;
//    }
//
//    @SerializedName("sunrise")
//    public long sunrise;
//    @SerializedName("sunset")
//    public long sunset;
//}
//
//class Coord {
//    @SerializedName("lon")
//    public float lon;
//
//    public float getLon() {
//        return lon;
//    }
//
//    public void setLon(float lon) {
//        this.lon = lon;
//    }
//
//    public float getLat() {
//        return lat;
//    }
//
//    public void setLat(float lat) {
//        this.lat = lat;
//    }
//
//    @SerializedName("lat")
//    public float lat;
//}
